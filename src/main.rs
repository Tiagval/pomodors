use std::thread::sleep;
use std::time;
use std::env::args;
use std::process::Command;
use figlet_rs::FIGfont;

//ANSI escape sequences
//https://gist.github.com/fnky/458719343aabd01cfb17a3a4f7296797
static ESC: &str = "\x1b[";
static CLEAR: &str = "2J";
static HIDE_CURSOR: &str = "?25l";
static RESET_CURSOR: &str = "H";

#[derive(PartialEq)]
enum Pomodoro {
    Work,
    Rest,
    Timer
}

fn total_to_min_sec(total: u32) -> (u32, u32){
    let min = total / 60;
    let sec = total - min * 60;

    (min, sec)
}

fn parse_arg(arg: Option<String>) -> u32{

    match arg{
        Some(val) => val.parse().unwrap_or(0),
        None => 0
    }
}

fn main() {
    let standard_font = FIGfont::standand().unwrap();
    let one_sec = time::Duration::from_millis(1000);
    let mut args = args().skip(1); //First arg is the name of the program
    let first_arg = args.next();
    let mut mode = Pomodoro::Timer;
    if first_arg.unwrap() == "p"{
        mode = Pomodoro::Work;
    }

    let work_len = 25 * 60;
    let rest_len = 5  * 60;

    '_loop: loop {
        let (message, time) = match mode{
            Pomodoro::Timer =>{
                let sec_arg = args.next();
                let mins = parse_arg(sec_arg);
                let third_arg = args.next();
                let secs = parse_arg(third_arg);

                let total = mins * 60 + secs;
                ("Timer finished", total)
            },
            Pomodoro::Work  => ("Take a break! ", work_len),
            Pomodoro::Rest =>  ("Back to work!", rest_len)
        };
        for counter in (0..=time).rev() { //= operator makes it an inclusive range
            let (min, sec) = total_to_min_sec(counter);
            print!("{ESC}{CLEAR}
                    {ESC}{HIDE_CURSOR}
                    {ESC}{RESET_CURSOR}");
            let text = format!("{:02}:{:02}",min, sec);
            println!("{}", standard_font.convert(&text).unwrap());
            if counter == 0{
                Command::new("/usr/bin/osascript").args(["/Users/tvale/Code/pomodors/talk.ascrpt", message]).output().expect("Failed to run osascript");
                if mode == Pomodoro::Timer {
                    break '_loop;
                }

                if mode == Pomodoro::Work{
                    mode = Pomodoro::Rest;
                }
                else{
                    mode = Pomodoro::Work;
                }
            }

            sleep(one_sec);
        }
    }
}
